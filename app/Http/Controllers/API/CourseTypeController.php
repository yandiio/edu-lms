<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CourseType;
use App\Http\Resources\CoursesTypeResource;

class CourseTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CoursesTypeResource::collection(CourseType::with('course')->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $crs = CourseType::create(
            [
                'category_name' => $request->category_name,
                'category_type' => $request->category_type,
            ]);

        return new CoursesTypeResource($crs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CourseType $course)
    {
        return new CoursesTypeResource($course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseType $id)
    {
        if($id->delete()){
            return response('Delete Success',200)->header('Content-type','json');
      }
    }
}

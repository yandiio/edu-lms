<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\Quiz;
use App\Http\Resources\QuizResource;
use Auth;
use App\Models\User;

class APIQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Quiz $quiz)
    {
        return QuizResource::collection($quiz->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'quiz_name' => 'required',
            'question_total' => 'required',
            'description' => 'required',
            'thumbnail' => 'required'
        ]);

        $quiz = Quiz::Create([
            'quiz_name' => $request->quiz_name,
            'question_total' => $request->question_total,
            'description' => $request->description,
            'thumbnail' => $request->email,
            'slug_title' => Str::slug($request->quiz_name),
        ]);

        return new QuizResource($quiz);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        return new QuizResource($quiz);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz)
    {
        if ($request->user()->id !== $quiz->user_id) {
            return response()->json(['error' => 'You can only edit your mentor'], 403);
        }

        $quiz->update($request->only(['quiz_name', 'question_total', 'description', 'thumbnail']));

        return new QuizResource($quiz);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quiz)
    {
        if ($quiz->delete()) {
            return response('done', 200)->header('Content-type', 'json');
        }else{
            return response('error',400);
        }
    }
}

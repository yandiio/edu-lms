<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rating;
use App\Models\Mentor;
use App\Http\Resources\RatingResource;

class RatingController extends Controller
{

    public function index()
    {
        return RatingResource::collection(Rating::with('mentor')->paginate(25));
    }


    public function store(Request $request, Mentor $mentor)
    {
        $rating = Rating::firstOrCreate(
            [
                'user_id' => $request->user()->id,
                'mentor_id' => $mentor->id,
            ],
            ['rating' => $request->rating]
        );

        return new RatingResource($rating);
    }

    public function show(Rating $rating){
        return new RatingResource($rating);
    }
}

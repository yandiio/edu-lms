<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mentor;
use Response;
use App\Http\Resources\MentorResource;
use Auth;
use App\Models\User;

class MentorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Mentor $mentor)
    {
        return MentorResource::collection($mentor->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name' => 'required',
            'username' => 'required',
            'password' => 'required',
            'email' => 'required',
            'phone_number' => 'required'
        ]);

        $mentor = Mentor::Create([
            'user_id' => auth()->user()->id,
            'name' => $request->name,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'id_level' => $request->input('id_level','2'),
        ]);

        return new MentorResource($mentor);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mentor $mentor)
    {
        return new MentorResource($mentor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mentor $mentor)
    {
        if($request->user()->id !== $mentor->user_id){
            return response()->json(['error' => 'You can only edit your mentor'],403);
        }

        $mentor->update($request->only(['nip','username','phone_number','password','email','name']));

        return new MentorResource($mentor);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mentor $mentor)
    {
        if($mentor->delete()){
              return response('done',200)->header('Content-type','json');
        }

    }

    public function getSearchResult(Request $request)
    {
        $search = $request->get('data');

        $drivers = Mentor::where('name','like',"{%$search%}")
                ->orWhere('username','like',"{%$search%}")
                ->get();
                    
               return Response([
                   'data' => $drivers
               ]);
    }
}

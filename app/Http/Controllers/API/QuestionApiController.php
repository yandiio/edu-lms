<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuizQuestion;
use App\Http\Resources\QuestionResource;
use Auth;
use App\Models\User;
use Symfony\Component\Console\Question\Question;

class QuestionApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(QuizQuestion $quiz)
    {
        return QuestionResource::collection($quiz->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'question' => 'required',
            'question_image' => 'required',
            'score' => 'required',
        ]);

        $quiz = QuizQuestion::Create([
            'question' => $request->quiz_name,
            'question_image' => $request->question_image,
            'score' => $request->score,
        ]);

        return new QuestionResource($quiz);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(QuizQuestion $quiz)
    {
        return new QuestionResource($quiz);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuizQuestion $quiz)
    {
        if ($request->user()->id !== $quiz->user_id) {
            return response()->json(['error' => 'You can only edit your mentor'], 403);
        }

        $quiz->update($request->only(['question', 'question_image', 'score']));

        return new QuizResource($quiz);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuizQuestion $quiz)
    {
        if ($quiz->delete()) {
            return response('done', 200)->header('Content-type', 'json');
        }
    }
}

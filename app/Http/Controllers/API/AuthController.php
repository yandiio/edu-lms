<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User;
use Auth;

class AuthController extends Controller
{
  public $successStatus = 200;
/**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['data' => $success], $this-> successStatus);
        }
        else{
            return response()->json(['data'=>'Unauthorised'], 401);
        }
    }
/**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'same_password' => 'required|same:password',
            'username' => 'required',
      ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

          $input = $request->all();

          $input['password'] = bcrypt($input['password']);
          $input['id_level'] = $request->input('id_level','1');
          $user = User::create($input);
          $success['token'] =  $user->createToken('MyApp')-> accessToken;
          $success['name'] =  $user->name;

          return response()->json(['success'=>$success], $this-> successStatus);
    }
/**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['data' => $user], $this-> successStatus);
    }

    public function reset()
    {

    }

    public function createVerifications()
    {
        
    }
}

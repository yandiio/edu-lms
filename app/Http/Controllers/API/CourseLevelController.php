<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CourseLevel;
use App\Http\Resources\CoursesLevelResource;

class CourseLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CoursesLevelResource::collection(CourseLevel::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'level_name' => 'required|min:3',
        ]);

        $couse = CourseLevel::Create([
            // 'user_id' => auth()->user()->id,
            'level_name' => $request->level_name,
        ]);

        return new CoursesLevelResource($couse);
    }   
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CourseLevel $crsLevel)
    {
        return new CoursesLevelResource($crsLevel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, CourseLevel $crs)
    {

        $mentor->update($request->only(['id','level_name']));

        return new CoursesLevelResource($crs);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseLevel $crs)
    {
        if($crs->delete()){
              return response('done',200)->header('Content-type','json');
        }
      
    }
}

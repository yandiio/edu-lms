<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Validator;
use App\Models\Course;
use App\Http\Resources\CoursesResource;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CoursesResource::collection(Course::with('mentor')->paginate(25));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     $file = $request->file('file');

         $this->validate($request,[
            'name_course' => 'required',
            'file' => 'required',
            'description' => 'required',
            'information' => 'required',
        ]);

         $nama_file = $file->getClientOriginalName();
         $tujuan_upload = 'tmp/thumbnail';
         $file->move($tujuan_upload,$nama_file);               


       
     $course = Course::Create([
        'name_course' => $request->name_course,
        'file' => $nama_file,
        'course_level_id' => $request->course_level_id,
        'course_type_id'=> $request->course_type_id,
        'mentor_id' => $request->mentor_id,
        'user_id' => auth()->user()->id,
        'description' => $request->description,
        'information' => $request->information,
        'slug_title' => Str::slug($request->name_course),
     ]);

     return new CoursesResource($course);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return new CoursesResource($course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
         if($request->user()->id !== $course->user_id){
            return response()->json(['error' => 'You can only edit your own mentor'],403);
        }

        $course->update($request->only(['description','information','name_course']));

        return new CoursesResource($course);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
          if($course->delete()){
              return response('Delete Success',200)->header('Content-type','json');
        }
    }

    public function getSearchResult(Request $request)
    {
        $data = $request->get('data');

        $drivers = Course::where('name_course', 'like', "%{$data}%")
                 ->orWhere('description', 'like', "%{$data}%")
                 ->get();
        
                 return response()->json([
                    'data' => $drivers
                 ],200);
    }
}
